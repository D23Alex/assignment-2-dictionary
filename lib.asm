section .text
 
global exit
global exit_return_code_zero
global string_length
global print_string
global print_error_string
global print_error_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define NULL_TERMINATOR 0
%define EXIT 60
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

exit_return_code_zero:
    xor rdi, rdi
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    
    .loop:
        cmp byte [rdi+rax], NULL_TERMINATOR
	je .end
	inc rax
	jmp .loop
    
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    ; задать длину строки
    call string_length
    mov rdx, rax
    pop rdi

    ; задать адрес строки
    mov rsi, rdi

    mov rax, 1; 'write' syscall number
    mov rdi, 1; to stdout

    syscall
    ret
    
print_error_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rdi
    mov rsi, rdi
    mov rax, 1; 'write' syscall number
    mov rdi, 2; to stderr
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ; задать длину строки
    mov rdx, 1
    
    ; задать адрес строки (код символа сохранить в стеке, получить доступ к адресу через вершину стека)
    push rdi
    mov rsi, rsp
    pop rdi

    ; задать system call number и descriptor
    mov rdi, 1
    mov rax, 1

    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r15
    
    mov rax, rdi; by requirements, the parameter is passed through rdi, but we need it in rax to do math
    mov r15, rsp; r11 - initial rsp
    mov rdi, rsp
    sub rdi, 500;
    xor r8, r8
    mov r10, 10
    
    .save_chars_loop:
    	xor rdx, rdx
    	div r10;
    	add rdx, 48
    	push rdx
    	test rax, rax
    	jne .save_chars_loop
    	
    .construct_str_loop:
    	pop rax
    	mov byte [rdi + r8], al
    	inc r8
    	cmp r15, rsp
    	jne .construct_str_loop
    
    mov byte [rdi + r8], NULL_TERMINATOR
    mov rsp, rdi
    call print_string
    mov rsp, r15
    
    pop r15
    
    ret
    	
    	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    or rdi, rdi
    jns .print_positive
    
    ; case negative - print "-" followed by rdi's two's complement negate
    push rdi
    mov rdi, 45
    call print_char
    pop rdi
    neg rdi
    
    .print_positive:
    call print_uint
    ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; takes string-pointers through rdi & rsi
    xor r8, r8; r8 - current symbol number
    mov rax, 1; rax - bool answer

    .current_char_compare_loop:
    	mov r9b, byte [rdi + r8]
	cmp byte [rsi + r8], r9b
	jne .make_false
	cmp r9b, 0
	je .return
	cmp byte [rsi + r8], 0
	je .return
	inc r8
	jmp .current_char_compare_loop
	.make_false:
	xor rax, rax

    .return:
    	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r14
    push r15

    xor r8, r8; r8 - current char number
    mov r14, rdi; r14 - buffer pointer
    mov r15, rsi
    dec r15 ; r15 - max length, 1 shy of buffer size because null-terminator is required 
    
    
    .skip_spaces_loop:
    	call read_char
    	cmp al, ' '
    	je .skip_spaces_loop
    	cmp al, `\t`
    	je .skip_spaces_loop
    	cmp al, `\n`
    	je .skip_spaces_loop
    
    .read_word_by_chars_loop:
    	; when we first enter that loop, al already contains a char that was read in .skip_sapces_loop,
    	; there's no need to read it again
    	cmp r8, 0
    	je .after_read_char
    	call read_char
    	.after_read_char:
    	; current char == 0 or 0x20 or 0x9 or 0xA means reading finished
    	cmp al, 0
    	je .success_finish
    	cmp al, ' '
    	je .success_finish
    	cmp al, `\t`
    	je .success_finish
    	cmp al, `\n`
    	je .success_finish
    	; current char number >= buffer length means not enough space in the buffer
    	cmp r8, r15
    	je .failure_finish
    	mov byte [r14 + r8], al
    	inc r8
    	jmp .read_word_by_chars_loop
    	
    .success_finish:
        mov byte [r14 + r8], 0
    	mov rdx, r8
    	mov rax, r14
    	pop r15
    	pop r14
    	ret
    	
    .failure_finish:
    	mov rax, 0
    	pop r15
    	pop r14
    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8; r8 - counter
    mov r10, 10; r10 - multiplyer
    xor r9, r9
    xor rax, rax
    
    .parse_loop:
    	mov r9b, byte [rdi + r8]
    	
    	; finish if current symbol is not a digit
    	cmp r9b, 57
    	ja .finish
    	cmp r9b, 48
    	jb .finish
    	
    	sub r9b, 48
    	mul r10
    	add rax, r9
    	inc r8
    	jmp .parse_loop
    
    .finish:
    	mov rdx, r8
    	ret
    
    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r9, r9;
    push rdi
    call parse_uint
    pop rdi
    cmp rdx, 0
    ; if success parsing the integer as unsigned, then it was positive, no minus sign
    jne .return
    mov r9b, byte [rdi]
    cmp r9b, 45
    ; if the integer isn't positive and there is no minus sign, number is incorrect, finish with rdx=0
    jne .return
    inc rdi
    call parse_uint
    test rdx, rdx
    ;if we can't parse the rest, number is incorrect, finish with rdx=0 
    je .return;
    neg rax
    inc rdx
    .return:
    	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax; rax - number of current char
    
    .copy_loop:
    	cmp rax, rdx
    	je .return_failure
    	mov r8b, byte [rdi + rax]
    	mov byte [rsi + rax], r8b
    	inc rax
    	cmp r8b, 0
    	jne .copy_loop
    	
    inc rax
    ret
    
    .return_failure:
    	xor rax, rax
    	ret
    	
