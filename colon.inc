%define first_list_element 0
; 1st argument is the key, 2nd argument is label
%macro colon 2
	%ifstr %1
		%ifid %2
			; put a link to element, that was previously first
			%2: dq first_list_element
			; write key of the element being added
			db %1, 0
			; redefine the first element
			%define first_list_element %2
		%else
			%error "2nd argument must be a label"
		%endif
	%else
		%error "1st argument must be a string"
	%endif
%endmacro
