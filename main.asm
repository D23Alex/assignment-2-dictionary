global _start

%include "colon.inc" 
%include "lib.inc"
%include "words.inc"
extern find_word

section .data
string: times 255 db 0
section .rodata
invalid_input_error_message: db "Invalid input", 0
key_not_found_message: db "Key not found", 0

%define LIST_ELEMENT_SIZE 9


section .text
_start:
	call read_256byte_string
	push rdx
	test rdx, rdx
	jz .case_invalid_input
	call find_string_in_dictionary
	test rax, rax
	jz .case_key_not_found
	
	.case_key_found:
	; rax contains address of the list element that was found
	; pointer to prev element of list is 8 bytes
	; the amount of bytes of key is on the stack, and 1 byte for a null-terminator
	; sum that all up to skip the prev element pointer and key to get the adress of value 
	pop rdi
	add rdi, rax
	add rdi, LIST_ELEMENT_SIZE
	call print_string
	
	.finish:
		call print_newline
		call exit_return_code_zero
	
	.case_invalid_input:
		call print_invalid_input_error
		jmp .finish
		
	.case_key_not_found:
		call print_key_not_found
		jmp .finish
		
		
print_invalid_input_error:
	mov rdi, invalid_input_error_message
	call print_error_string
	ret


print_key_not_found:
	mov rdi, key_not_found_message
	call print_string
	ret
		
		
find_string_in_dictionary:
	mov rdi, rax
	mov rsi, first_list_element
	call find_word
	ret
	

read_256byte_string:
	mov rdi, string
	mov rsi, 255
	call read_word
	ret

