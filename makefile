ASM=nasm
ASMFLAGS=-f elf64
LD=ld
LDFLAGS=-o
RM=rm
RMFLAGS=-rf

.PHONY: clean run

run: clean main

main: main.o dict.o lib.o
	$(LD) $(LDFLAGS) $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@
	
clean:
	$(RM) $(RMFLAGS) *.o main
	

