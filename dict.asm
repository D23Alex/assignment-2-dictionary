extern string_equals

section .text

global find_word

%define POINTER_SIZE 0x8

find_word:
.loop:
	push rdi
	push rsi
	
	.compare_with_current_node:
		add rsi, POINTER_SIZE       
		call string_equals
		pop rsi
		pop rdi
		cmp rax, 0x1
		je .case_node_found
	
	.move_to_next_node_or_fail_if_none:
		mov rsi, [rsi]
		test rsi, rsi
		jnz .loop
	
	.case_node_not_found:
		xor rax, rax          
		ret
	
	.case_node_found:
		mov rax, rsi
		ret

